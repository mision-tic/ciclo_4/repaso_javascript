/*console.log("paso 1");
for (let i = 0; i < 10; i++) {
    console.log("Ejecutando proceso...");
}

console.log("paso 2");*/



function proceso() {
    
    for (let i = 0; i < 10; i++) {
        console.log("Ejecutando proceso...");
    }
}

const promesa = () => new Promise( () => setTimeout(proceso, 2000)  );


console.log("paso 1");
promesa().then(()=>{
console.log("paso 2");
console.log("paso 3");
console.log("paso 4");
}).catch(error =>{
    console.log("error");
})

//------------------------------------

function saludarFunc(){
    return "Hola mundo desde una función";
}

const saludar = () => "Hola mundo";

const sumar = (num1, num2) => {
    let resultado = num1 + num2;
    return resultado;
}

saludarFunc();

console.log(saludarFunc());

console.log(saludar());

let resultado = sumar(5,10);
console.log(resultado);